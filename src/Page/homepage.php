<?php

echo "
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Sklep Muzyczny Konsi</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            line-height: 1.8;
            margin: 24px;
        }
        header {
            text-align: center;
            padding: 24px;
            background-color: #f4f4f4;
        }
        main {
            max-width: 880px;
            margin: 0 auto;
            text-align: center;
        }
        h1, h2 {
            color: #333;
        }
        p {
            color: #666;
        }
        #ofertaButton {
            display: inline-block;
            padding: 26px;
            font-size: 20px;
            background-color: #4CAF50;
            color: blue;
            text-decoration: none;
            border-radius: 10px;
            margin-top: 24px;
        }
        .list-container {
            text-align: left;
        }
        .list-item {
            margin-bottom: 14px;
        }
    </style>
</head>
<body>

    <header>
        <h1>Witaj w Naszym Sklepie Muzycznym!</h1>
        <p>Ciesz się z łatwego i wygodnego zakupu w najlepszym sklepie muzycznym w całym internecie!</p>
    </header>

    <main>
        <section>
            <h2>Dlaczego warto wybrać nasz sklep?</h2>
            <div class='list-container'>
                <div class='list-item'>
                    <strong>Bogaty Wybór Produktów:</strong> Oferujemy szeroką gamę wysokiej jakości produktów dla profesjonalistów.
                </div>
                <div class='list-item'>
                    <strong>Szybka Dostawa:</strong> Gwarantujemy ekspresową dostawę, abyś mógł cieszyć się zakupami jak najszybciej.
                </div>
                <div class='list-item'>
                    <strong>Konkurencyjne Ceny:</strong> Nasze ceny są atrakcyjne, co sprawia, że nie przepłacasz.
                </div>
                <div class='list-item'>
                    <strong>Profesjonalne Doradztwo:</strong> Nasz zespół ekspertów służy profesjonalnym doradztwem, abyś dokonał najlepszego wyboru.
                </div>
            </div>
        </section>

        <!-- Dodaj przycisk oferty -->
        <a href='?page=oferta' id='ofertaButton'>OFERTA</a>
    </main>

</body>
</html>
";
?>

