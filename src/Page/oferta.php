<?php

namespace App;

// Importuj klasę Database
use App\Database;

// Utwórz obiekt klasy Database
$database = new Database();

// Utwórz tabelę "products", jeśli nie istnieje
$database->createProductsTable();

// Dodaj przykładowe produkty, jeśli tabela jest pusta
$existingProducts = $database->select('products');
if (empty($existingProducts)) {
    $database->addSampleProducts();
}

// Wybierz produkty z tabeli "products"
$products = $database->select('products');

// Obsłuż przesłane zamówienie
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['deleteOrder'])) {
        // Usuwanie zamówienia
        $orderId = $_POST['orderId'];
        $database->delete('orders', "id = $orderId");
    } elseif (isset($_POST['updateOrder'])) {
        // Aktualizacja ilości zamówionego produktu
        $orderId = $_POST['orderId'];
        $newQuantity = $_POST['updateQuantity'];
        $database->edit('orders', "id = $orderId", ['quantity' => $newQuantity]);
    } else {
        // Dodawanie nowego zamówienia
        $productId = $_POST['product'] ?? null;
        $quantity = $_POST['quantity'] ?? 1;

        // Pobierz szczegóły zamówionego produktu
        $orderedProduct = $database->select('products', "id = $productId")[0] ?? null;

        // Dodaj zamówienie do bazy danych
        if ($orderedProduct) {
            $database->addOrder($orderedProduct['name'], $orderedProduct['price'], $quantity);
        }
    }
}

// Wybierz zamówienia z tabeli "orders"
$orders = $database->select('orders');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formularz zamówienia</title>
</head>
<body>

<p>Proszę wybrać produkt z listy:</p>

<form action="" method="post"> <!-- Pusty action oznacza, że formularz zostanie przesłany na tę samą stronę -->
    <label for="product">Produkt:</label>
    <select name="product" id="product">
        <?php foreach ($products as $product): ?>
            <option value="<?php echo htmlspecialchars($product['id']); ?>">
                <?php echo htmlspecialchars($product['name']) . ' - ' . htmlspecialchars($product['price']) . ' zł'; ?>
            </option>
        <?php endforeach; ?>
    </select>

    <label for="quantity">Ilość:</label>
    <input type="number" name="quantity" id="quantity" value="1" min="1">

    <button type="submit">Zamów</button>
</form>

<!-- Wyświetl składane zamówienia -->
<h2>Składane zamówienia:</h2>
<?php
$totalNetAmount = 0;
$totalGrossAmount = 0;

foreach ($orders as $order):
    $productName = htmlspecialchars($order['product_name']);
    $quantity = htmlspecialchars($order['quantity']);
    $totalPrice = htmlspecialchars($order['total_price']);

    // Oblicz łączną cenę netto i brutto zamówionych towarów
    $netPrice = $totalPrice / 1.2; // Cena netto (80% wartości)
    $grossPrice = $totalPrice - $netPrice; // Podatek (20% wartości)

    $totalNetAmount += $netPrice * $quantity;
    $totalGrossAmount += $totalPrice * $quantity;
    ?>
    <form action="" method="post">
        <input type="hidden" name="orderId" value="<?php echo $order['id']; ?>">
        <p>
            <?php echo "{$productName} - {$quantity} szt. - {$totalPrice} zł "; ?>
            <button type="submit" name="deleteOrder">Usuń</button>
            <label for="updateQuantity">Ilość:</label>
            <input type="number" name="updateQuantity" id="updateQuantity" value="<?php echo $order['quantity']; ?>" min="1">
            <button type="submit" name="updateOrder">Zaktualizuj</button>
        </p>
    </form>
<?php endforeach; ?>

<!-- Wyświetl łączną cenę netto i brutto -->
<p>Łączna cena netto: <?php echo round($totalNetAmount, 2); ?> zł</p>
<p>Łączna cena brutto: <?php echo round($totalGrossAmount, 2); ?> zł</p>

<!-- Dodaj przycisk "Wyślij zamówienie" -->
<button type="button" onclick="showShippingForm()">Wyślij zamówienie</button>

<!-- Formularz do podania danych do wysyłki (domyślnie ukryty) -->
<div id="shippingForm" style="display: none;">
    <h2>Dane do wysyłki</h2>
    <form action="?page=user" method="post">
        <label for="firstName">Imię:</label>
        <input type="text" name="firstName" required>

        <label for="lastName">Nazwisko:</label>
        <input type="text" name="lastName" required>

        <label for="street">Ulica:</label>
        <input type="text" name="street" required>

        <label for="houseNumber">Numer domu:</label>
        <input type="number" name="houseNumber" required>

        <label for="phoneNumber">Numer telefonu:</label>
        <input type="tel" name="phoneNumber" required>

        <button type="submit">Wyślij zamówienie</button>
    </form>
</div>

<script>
    function showShippingForm() {
        // Po kliknięciu przycisku "Wyślij zamówienie", pokaż formularz danych do wysyłki
        document.getElementById('shippingForm').style.display = 'block';
    }
</script>

</body>
</html>
