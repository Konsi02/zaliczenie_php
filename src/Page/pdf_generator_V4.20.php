<?php

global $database;
require_once('../vendor/autoload.php');
require_once '../vendor/tecnickcom/tcpdf/tcpdf.php';

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['generatePDF'])) {
    // Pobieranie danych z formularza
    $firstName = $_POST['firstName'] ?? '';
    $lastName = $_POST['lastName'] ?? '';
    $street = $_POST['street'] ?? '';
    $houseNumber = $_POST['houseNumber'] ?? '';
    $phoneNumber = $_POST['phoneNumber'] ?? '';
    $totalOrderPrice = $_POST['totalOrderPrice'] ?? '';

    // Utwórz nowy obiekt klasy TCPDF
    $pdf = new TCPDF();

    // Ustawienia PDF
    $pdf->SetAutoPageBreak(true);
    $pdf->AddPage();

    // Ustawienie czcionki z polskimi znakami
    $pdf->SetFont('dejavusans', '', 12);

    // Dodaj nagłówek faktury
    $pdf->SetFont('dejavusans', 'B', 16);
    $pdf->Cell(0, 10, 'Faktura do zamówienia', 0, 1, 'C');

    // Dodaj dane do PDF
    $pdf->SetFont('dejavusans', '', 12);
    $pdf->Cell(0, 10, 'Dane do wysyłki:', 0, 1);
    $pdf->Cell(0, 10, "Imię: $firstName", 0, 1);
    $pdf->Cell(0, 10, "Nazwisko: $lastName", 0, 1);
    $pdf->Cell(0, 10, "Ulica: $street", 0, 1);
    $pdf->Cell(0, 10, "Numer domu: $houseNumber", 0, 1);
    $pdf->Cell(0, 10, "Numer telefonu: $phoneNumber", 0, 1);




    $pdf->Cell(0, 10, 'Składane zamówienia:', 0, 1);
    $orders = $database->select('orders');

    foreach ($orders as $order) {
        $productName = htmlspecialchars($order['product_name']);
        $quantity = htmlspecialchars($order['quantity']);
        $totalPrice = htmlspecialchars($order['total_price']);

        $pdf->Cell(0, 10, "{$productName} - {$quantity} szt. - {$totalPrice} zł", 0, 1);
    }

    $pdf->Cell(0, 10, "Łączny koszt zamówienia: $totalOrderPrice zł", 0, 1);

    // Zapisz PDF do pliku
    $pdfFileName = '/var/www/php-wprowadzenie/order_summary.pdf';
    $pdf->Output($pdfFileName, 'I');

    // Wyświetl komunikat
    echo "Wygenerowano fakturę do zamówienia. [ <a href='$pdfFileName'>Pobierz fakturę</a> ]";
}

?>




