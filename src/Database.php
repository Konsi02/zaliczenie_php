<?php

namespace App;

class Database
{
    private string $hostname = 'localhost';
    private string $dbname = 'example_database';
    private string $username = 'user';
    private string $password = '12345';
    private \PDO $conn;

    public function __construct()
    {
        try {
            $this->conn = new \PDO(
                "mysql:host=$this->hostname;dbname=$this->dbname",
                $this->username,
                $this->password
            );
            // Ustaw tryb błędu PDO na wyjątek
            $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
            exit;
        }
    }

    public function select(string $table, string $condition = null)
    {
        $sql = "SELECT * FROM $table";
        if ($condition) {
            $sql .= " WHERE $condition";
        }

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
            return [];
        }
    }

    public function delete(string $table, string $condition)
    {
        $sql = "DELETE FROM $table WHERE $condition";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            return $stmt->rowCount();
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
            return 0;
        }
    }

    public function edit(string $table, string $condition, array $params)
    {
        $sql = "UPDATE $table SET";
        $updates = [];

        foreach ($params as $key => $value) {
            $updates[] = "$key = :$key";
        }

        $sql .= implode(', ', $updates);
        $sql .= " WHERE $condition";

        try {
            $stmt = $this->conn->prepare($sql);
            foreach ($params as $key => &$value) {
                $stmt->bindParam(":$key", $value);
            }
            $stmt->execute();
            return $stmt->rowCount();
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
            return 0;
        }
    }
    public function addShippingData($firstName, $lastName, $street, $houseNumber, $phoneNumber)
    {
        $sql = "INSERT INTO shipping_data (first_name, last_name, street, house_number, phone_number)
            VALUES (:firstName, :lastName, :street, :houseNumber, :phoneNumber)";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':firstName', $firstName);
            $stmt->bindParam(':lastName', $lastName);
            $stmt->bindParam(':street', $street);
            $stmt->bindParam(':houseNumber', $houseNumber);
            $stmt->bindParam(':phoneNumber', $phoneNumber);
            $stmt->execute();
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function createShippingDataTable()
    {
        $sql = "CREATE TABLE IF NOT EXISTS shipping_data (
        id INT PRIMARY KEY AUTO_INCREMENT,
        first_name VARCHAR(255) NOT NULL,
        last_name VARCHAR(255) NOT NULL,
        street VARCHAR(255) NOT NULL,
        house_number VARCHAR(10) NOT NULL,
        phone_number VARCHAR(15) NOT NULL
    )";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function createProductsTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS products (
            id INT PRIMARY KEY AUTO_INCREMENT,
            name VARCHAR(255) NOT NULL,
            price DECIMAL(10, 2) NOT NULL
        )";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function createOrdersTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS orders (
            id INT PRIMARY KEY AUTO_INCREMENT,
            product_name VARCHAR(255) NOT NULL,
            quantity INT NOT NULL,
            total_price DECIMAL(10, 2) NOT NULL
        )";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    // Metody odpowiedzialne za zarządzanie danymi wysyłkowymi, produktami i zamówieniami
  

    public function addSampleProducts()
    {
        // Dodaj przykładowe produkty muzyczne, jeśli tabela jest pusta
        $existingProducts = $this->select('products');
        if (empty($existingProducts)) {
            $this->addProduct('Gitara elektryczna Fender', 1200.00);
            $this->addProduct('Klawisze Yamaha P-45', 450.99);
            $this->addProduct('Mikrofon Shure SM58', 99.99);
            $this->addProduct('Interfejs audio Focusrite Scarlett 2i2', 149.99);
            $this->addProduct('Słuchawki Audio-Technica ATH-M50x', 149.00);
            $this->addProduct('Perkusja akustyczna Pearl', 699.99);
            $this->addProduct('Wzmacniacz gitarowy Marshall', 399.99);
            $this->addProduct('Efekt gitarowy Boss DD-7', 159.99);
            $this->addProduct('Struny do gitary Ernie Ball', 9.99);
            $this->addProduct('Statyw mikrofonowy', 29.99);
            $this->addProduct('Pianino cyfrowe Casio', 899.99);
            $this->addProduct('Ukulele Kala', 59.99);
            $this->addProduct('Saksofon altowy Yamaha', 1200.99);
            $this->addProduct('Djembe', 129.99);
        }
    }

}

public function addProduct($name, $price): void
{
    $sql = "INSERT INTO products (name, price) VALUES (:name, :price)";

    try {
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':price', $price);
        $stmt->execute();
    } catch (\PDOException $e) {
        echo 'Error: ' . $e->getMessage();
    }
}

public function addOrder($productName, $productPrice, $quantity): void
{
    $totalPrice = $productPrice * $quantity;

    $sql = "INSERT INTO orders (product_name, quantity, total_price) VALUES (:productName, :quantity, :totalPrice)";

    try {
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':productName', $productName);
        $stmt->bindParam(':quantity', $quantity);
        $stmt->bindParam(':totalPrice', $totalPrice);
        $stmt->execute();
    } catch (\PDOException $e) {
        echo 'Error: ' . $e->getMessage();
    }
}

