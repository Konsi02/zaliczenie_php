<?php

namespace App;

class App
{
    private string $page;
    private Layout $layout;
    private array $allowedPages = [
        'homepage',
        'oferta',
        'user',
        'submit_order',
        'generate_pdf'
    ];

    public function run(): void
    {
        $this->parseRouting();
        $this->initializeLayout();
        $this->layout->render();
    }

    private function parseRouting(): void
    {
        $this->page = $this->getPageFromRequest('homepage');
    }

    private function getPageFromRequest(string $defaultPage): string
    {
        $page = $_GET['page'] ?? $defaultPage;
        return in_array($page, $this->allowedPages) ? $page : $defaultPage;
    }

    private function initializeLayout(): void
    {
        $this->layout = new Layout($this->page, 'upsl');
    }
}
