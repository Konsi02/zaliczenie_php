<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            line-height: 1.8;
            margin: 24px;
        }
        h1 {
            color: #400;
        }
        a {
            margin-right: 12px;
            text-decoration: none;
            color: #007bff;
        }
    </style>
</head>
<body>

<h1>Sklep Muzyczny "Konsi"</h1>
<a href="/?page=homepage">homepage</a>
<a href="/?page=oferta">Oferta</a>

<div>
    <?php echo $content ?>
</div>

</body>
</html>

