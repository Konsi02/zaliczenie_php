<?php

namespace App;

class Layout
{
    private string $page;
    private string $layoutName;
    private string $title = 'Sklep internetowy';

    public function __construct(string $page, string $layoutName)
    {
        $this->page = $page;
        $this->layoutName = $layoutName;
    }

    public function render()
    {
        $title = $this->title;
        $content = $this->renderPage();

        $layoutPath = __DIR__ . "/layouts/{$this->layoutName}.php";
        if (file_exists($layoutPath)) {
            require $layoutPath;
        } else {
            // Obsługa błędu: plik layoutu nie istnieje
            echo "Błąd: nie znaleziono layoutu '{$this->layoutName}'.";
        }
    }

    private function renderPage(): string
    {
        $pagePath = __DIR__ . "/page/{$this->page}.php";
        if (file_exists($pagePath)) {
            ob_start();
            require $pagePath;
            return ob_get_clean();
        } else {
            // Obsługa błędu: plik strony nie istnieje
            return "Błąd: nie znaleziono strony '{$this->page}'.";
        }
    }
}
